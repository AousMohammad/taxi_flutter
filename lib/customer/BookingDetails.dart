import 'package:app/customer/BookingTaxi.dart';
import 'package:app/model/Prepare.dart';
import 'package:app/model/customer/Taxi.dart';
import 'package:app/services/Auth.dart';
import 'package:app/model/customer/ListTaxi.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BookingDetails extends StatefulWidget {

  @override
  State<BookingDetails> createState() => _BookingDetailsState();

}

class _BookingDetailsState extends State<BookingDetails> {
  bool isPasswordVisible = false;
  String myLocation;
  String destination;

  TextEditingController _site = TextEditingController();
  TextEditingController _destination = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue, Colors.blueGrey])),
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.fromLTRB(15, 80, 15, 0),
              child: Form(
                key: _formKey,
                child: Column(children: [
                  Align(
                  alignment: Alignment.topRight,
                    child: Container(
                    height: 100,
                    width: 300,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Colors.blue, Colors.blueGrey]),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 4,
                              spreadRadius: 3,
                              color: Colors.black12)
                        ],
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(200),
                            bottomRight: Radius.circular(200))),
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 32, left: 70),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            " Booking",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                shadows: [
                                  Shadow(
                                      color: Colors.black45,
                                      offset: Offset(1, 1),
                                      blurRadius: 5)
                                ]),
                          ),
                          Text(
                            " Details",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                shadows: [
                                  Shadow(
                                      color: Colors.black45,
                                      offset: Offset(1, 1),
                                      blurRadius: 5)
                                ]),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 80,
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 30).copyWith(bottom: 10),
                  child: TextFormField(
                    controller: _site,
                    validator: (val) {
                      myLocation = val;
                      return val.isEmpty ? "Write the site " : null;
                    },
                    style: TextStyle(color: Colors.white, fontSize: 14.5),
                    decoration: InputDecoration(
                        prefixIconConstraints: BoxConstraints(minWidth: 45),
                        prefixIcon: Icon(
                          Icons.location_on,
                          color: Colors.white70,
                          size: 24,
                        ),
                        border: InputBorder.none,
                        hintText: '  Enter your site',
                        hintStyle:
                            TextStyle(color: Colors.white60, fontSize: 15),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white38)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white70))),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 30).copyWith(bottom: 10),
                  child: TextFormField(
                    controller: _destination,
                    validator: (val) {
                      destination = val;
                      return val.isEmpty ? "Write the destination " : null;
                    },
                    style: TextStyle(color: Colors.white, fontSize: 14.5),
                    decoration: InputDecoration(
                        prefixIconConstraints: BoxConstraints(minWidth: 45),
                        prefixIcon: Icon(
                          Icons.location_on_outlined,
                          color: Colors.white70,
                          size: 24,
                        ),
                        border: InputBorder.none,
                        hintText: '  Where are you going',
                        hintStyle:
                            TextStyle(color: Colors.white60, fontSize: 15),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white38)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white70))),
                  ),
                ),
                SizedBox(
                  height: 100,
                ),
                GestureDetector(
                  onTap: () async{
                    if (_formKey.currentState.validate()) {
                     await location();
                     listCar();
                    }
                  },
                  child: Container(
                    height: 70,
                    width: 250,
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 4,
                              color: Colors.black12.withOpacity(.2),
                              offset: Offset(2, 2))
                        ],
                        gradient: LinearGradient(
                            colors: [Colors.blue, Colors.blueGrey])),
                    child: Text('Confirm',
                        style: TextStyle(
                            color: Colors.white.withOpacity(.8),
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ]),
            ),
          ),
        ),
        ),
      ),
    );
  }

  Future listCar() async {

    ListTaxi response = await Provider.of<Auth>(context, listen: false)
        .customer_get_nearly_cars();

    if (response.status) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => BookingTaxi(myLocation, destination),
        ),
      );
    }
  }

  Future location() async {
    Map users = {
      'location': this.myLocation,
    };
    Prepare response = await Provider.of<Auth>(context, listen: false)
        .perpare_account(loc: users);

  }

}
