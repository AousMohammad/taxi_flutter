import 'package:app/driver/DriverInfo.dart';
import 'package:app/model/Message.dart';
import 'package:app/model/customer/getOrder.dart';
import 'package:app/model/customer/Orders.dart';
import 'package:app/model/user/User.dart';
import 'package:app/services/Auth.dart';
import 'package:app/model/customer/Taxi.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BookingTaxi extends StatefulWidget {
  String myLocation;
  String destination;

  BookingTaxi(this.myLocation, this.destination);

  @override
  State<BookingTaxi> createState() => _BookingTaxiState();
}

class _BookingTaxiState extends State<BookingTaxi> {
  bool isPasswordVisible = false;


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue, Colors.blueGrey])),
          child: Consumer<Auth>(builder: (context, auth, child) {
            return ListView.builder(
                    itemCount: auth.taxis == null ? 0 : auth.taxis.length,
                    itemBuilder: (ctx, index) {
                      return Padding(
                        padding: EdgeInsets.fromLTRB(50, 50, 50, 0),
                        child: Card(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, top: 20),
                                child: ListTile(
                                  leading: Icon(
                                    Icons.local_taxi,
                                    size: 80,
                                    color: Colors.yellow,
                                  ),
                                  title: Text(
                                      auth.taxis[index].model,
                                      style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10),
                                child: TextButton(
                                  child: Text(
                                    'Select',
                                    style: TextStyle(
                                      color: Colors.blue,
                                      fontSize: 15,
                                    ),
                                  ),
                                  onPressed: () async {
                                    await details(auth.taxis[index].driver_id);
                                    Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => DriverInfo(),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                          //    ),
                        ),
                      );
                    },
                  );
                }),
          ),
    ),
    );
  }

  Future details(int index) async {
    Map ord = {
      'driver_id': index,
      'start': this.widget.myLocation,
      'destination': this.widget.destination,
    };

    getOrder response = await Provider.of<Auth>(context, listen: false)
        .customer_order_taxi(ord: ord);
    Message driver =
        await Provider.of<Auth>(context, listen: false).driver_info(index);
  }

}
