import 'package:app/model/Prepare.dart';
import 'package:app/services/Auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'DriverChoose.dart';
import '../Home.dart';

class DriverFree extends StatefulWidget {
  @override
  State<DriverFree> createState() => _DriverFreeState();
}

class _DriverFreeState extends State<DriverFree> {
  bool freeSelected = false;
  bool unFreeSelected = false;
  bool display = false;

  TextEditingController _location = TextEditingController();
  TextEditingController _model = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _location.dispose();
    _model.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue, Colors.blueGrey])),

          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.fromLTRB(15, 60, 15, 0),
              child: Form(
                key: _formKey,
                child: Column(children: [
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      height: 95,
                      width: 300,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Colors.blue, Colors.blueGrey]),
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 4,
                                spreadRadius: 3,
                                color: Colors.black12)
                          ],
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(200),
                              bottomRight: Radius.circular(200))),
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 30, left: 60),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              'Driver',
                              style: TextStyle(
                                  fontSize: 23,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  shadows: [
                                    Shadow(
                                        color: Colors.black45,
                                        offset: Offset(1, 1),
                                        blurRadius: 5)
                                  ]),
                            ),
                            Text(
                              ' Free',
                              style: TextStyle(
                                  fontSize: 23,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue,
                                  shadows: [
                                    Shadow(
                                        color: Colors.black45,
                                        offset: Offset(1, 1),
                                        blurRadius: 5)
                                  ]),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(100, 30, 15, 0),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          freeSelected = true;
                          unFreeSelected = false;
                          display = false;
                        });
                      },
                      child: Row(
                        children: [
                          Container(
                              height: 20,
                              width: 20,
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(right: 10),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(color: Colors.white60)),
                              child: freeSelected
                                  ? Container(
                                      margin: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white70),
                                    )
                                  : SizedBox()),
                          Text('Unactive',
                              style: TextStyle(
                                  color: Colors.white70, fontSize: 25))
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(100, 25, 15, 0),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          unFreeSelected = true;
                          freeSelected = false;
                          display = true;
                        });
                      },
                      child: Row(
                        children: [
                          Container(
                              height: 20,
                              width: 20,
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(right: 10),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(color: Colors.white60)),
                              child: unFreeSelected
                                  ? Container(
                                      margin: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white70),
                                    )
                                  : SizedBox()),
                          Text('Active',
                              style: TextStyle(
                                  color: Colors.white70, fontSize: 25))
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 50,),
                  display ? Container(
                          width: 300,
                          child:
                              Column(mainAxisSize: MainAxisSize.max, children: [
                            Card(
                              child: ListTile(
                                leading: Icon(
                                  Icons.local_activity_outlined,
                                  size: 30,
                                  color: Colors.blue,
                                ),
                                title:
                                TextFormField(
                                    controller: _location,
                                    validator: (val) {
                                      return val.isEmpty
                                          ? "Write the Location "
                                          : null;
                                    },
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(
                                        color: Colors.blueGrey, fontSize: 14.5),
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'Location',
                                      hintStyle: TextStyle(
                                          color: Colors.blue, fontSize: 14.5),
                                    ),
                                ),
                              ),
                            ),
                            Container(
                              width: 300,
                              child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Card(
                                      child: ListTile(
                                        leading: Icon(
                                          Icons.car_rental,
                                          size: 30,
                                          color: Colors.blue,
                                        ),
                                        title: TextFormField(
                                            controller: _model,
                                            validator: (val) {
                                              return val.isEmpty
                                                  ? "Write the model "
                                                  : null;
                                            },
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(
                                                color: Colors.blueGrey,
                                                fontSize: 14.5),
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: 'Set model of your car',
                                              hintStyle: TextStyle(
                                                  color: Colors.blue,
                                                  fontSize: 14),
                                            )),
                                      ),
                                    ),
                                  ]),
                            ),
                          ]),
                        ) : SizedBox(),
                  SizedBox(
                    height: 50,
                  ),
                  GestureDetector(
                    onTap: () {
                      details();
                    },
                    child: Container(
                      height: 70,
                      width: 250,
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 4,
                                color: Colors.black12.withOpacity(.2),
                                offset: Offset(2, 2))
                          ],
                          gradient: LinearGradient(
                              colors: [Colors.blue, Colors.blueGrey])),
                      child: Text('Confirm',
                          style: TextStyle(
                              color: Colors.white.withOpacity(.8),
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                ]),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future details() async {
    Map free = {
      'location': _location.text,
      'model': _model.text,
    };
    Prepare response =
        await Provider.of<Auth>(context, listen: false).driver_free(free: free);

    print(_location.text);


    if(response.status)
   {
     if (_formKey.currentState.validate()) {
      freeSelected == false
          ? Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => DriverChoose(),
        ),
      )
          : Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Home(),
        ),
      );
    }
  }
  }
}
