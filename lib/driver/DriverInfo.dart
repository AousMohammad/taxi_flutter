import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app/services/Auth.dart';

class DriverInfo extends StatefulWidget {
  @override
  State<DriverInfo> createState() => _DriverInfoState();
}

class _DriverInfoState extends State<DriverInfo> {
  bool isPasswordVisible = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue, Colors.blueGrey])),
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.fromLTRB(15, 50, 15, 0),
              child: Column(children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    height: 100,
                    width: 300,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Colors.blue, Colors.blueGrey]),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 4,
                              spreadRadius: 3,
                              color: Colors.black12)
                        ],
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(200),
                            bottomRight: Radius.circular(200))),
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 35, left: 75),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            ' Driver',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                shadows: [
                                  Shadow(
                                      color: Colors.black45,
                                      offset: Offset(1, 1),
                                      blurRadius: 5)
                                ]),
                          ),
                          Text(
                            ' Info',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                shadows: [
                                  Shadow(
                                      color: Colors.black45,
                                      offset: Offset(1, 1),
                                      blurRadius: 5)
                                ]),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 70,),
                Consumer<Auth> (
                  builder: (context, auth, child) {
                  return Container(
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'images/person.jpg',
                                    ),
                                    SizedBox(height: 60,),
                                    Row (children: [
                                      Text(
                                      '        Driver name : ',
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.white,
                                    ),),

                                      Text(
                                       auth.drivInfo.user.name,
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                      ),),
                                    ]),
                                    SizedBox(height: 20,),
                                    Text(
                                      'Number : ' + auth.drivInfo.user.numberPhone,
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                      ),
                                    ),
                                    SizedBox(height: 20,),
                                    Text(
                                      '   Email : ' +auth.drivInfo.user.email,
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],),
                              );
                  }
                ),
              ]),
            ),
          ),
        ),
      ),
    );
  }

}
