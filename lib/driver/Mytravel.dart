import 'package:app/driver/DriverChoose.dart';
import 'package:app/model/Prepare.dart';
import 'package:app/services/Auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyTravel extends StatefulWidget {
  @override
  State<MyTravel> createState() => _MyTravelState();
}

class _MyTravelState extends State<MyTravel> {
  bool isPasswordVisible = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue, Colors.blueGrey])),
          child: Consumer<Auth>(builder: (context, auth, child) {
              return ListView.builder(
                itemCount: auth.travels == null ? 0 : auth.travels.length,
                itemBuilder: (ctx, index) {
                  return Padding(
                    padding: EdgeInsets.fromLTRB(25, 50, 20, 0),
                    child: Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          ListTile(
                            leading: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                              child: Icon(
                                Icons.emoji_transportation,
                                size: 60,
                                color: Colors.blueGrey,
                              ),
                            ),
                            title: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 20, 10, 0),
                              child: Column(
                                children: [
                                  Text('Price : '
                                      + auth.travels[index].price.toString() ,
                                      style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Text(
                                      'destination : ' +
                                          auth.travels[index].destination,
                                      style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: TextButton(
                              child: Text(
                                'Done !',
                                style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 15,
                                ),
                              ),
                              onPressed: () {
                                delete(auth.travels[index].id);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => DriverChoose(),
                                  ),);
                              },),
                          ),
                        ],),
                    ),
                  );
                });
          }),
        ),
      ),
    );
  }

  Future delete(int id) async {
    Prepare response = await Provider.of<Auth>(context, listen: false)
        .delete_travel(id);
  }

}
