import 'package:app/customer/CustomerInfo.dart';
import 'package:app/model/Message.dart';
import 'package:app/services/Auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Price extends StatefulWidget {
   int id;
  Price(this.id);

  @override
  State<Price> createState() => _PriceState();
}

class _PriceState extends State<Price> {
  bool isPasswordVisible = false;
  String myLocation;
  String destination;
  String price;
  TextEditingController _price = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue, Colors.blueGrey])),
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.fromLTRB(15, 80, 15, 0),
              child: Form(
                key: _formKey,
                child: Column(children: [
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      height: 100,
                      width: 300,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Colors.blue, Colors.blueGrey]),
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 4,
                                spreadRadius: 3,
                                color: Colors.black12)
                          ],
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(200),
                              bottomRight: Radius.circular(200))),
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 32, left: 70),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              " Order",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  shadows: [
                                    Shadow(
                                        color: Colors.black45,
                                        offset: Offset(1, 1),
                                        blurRadius: 5)
                                  ]),
                            ),
                            Text(
                              " price",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue,
                                  shadows: [
                                    Shadow(
                                        color: Colors.black45,
                                        offset: Offset(1, 1),
                                        blurRadius: 5)
                                  ]),
                            ),
                          ],),
                      ),
                    ),
                  ),
                  SizedBox(height: 100,),

                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 40)
                        .copyWith(bottom: 10),
                    child: TextFormField(
                      controller: _price,
                      validator: (val) {
                        price = val;
                        return val.isEmpty ? "Write the price " : null;
                      },
                      keyboardType: TextInputType.number,
                      style: TextStyle(color: Colors.white, fontSize: 20),
                      decoration: InputDecoration(
                          prefixIconConstraints: BoxConstraints(minWidth: 45),
                          prefixIcon: Icon(
                            Icons.monetization_on,
                            color: Colors.white70,
                            size: 32,
                          ),
                          border: InputBorder.none,
                          hintText: '   Enter price',
                          hintStyle:
                              TextStyle(color: Colors.white60, fontSize: 15),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white38)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70))),
                    ),
                  ),
                  SizedBox(height: 50),
                   GestureDetector(
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          priceOrder(price);
                        }
                      },
                      child: Container(
                        height: 70,
                        width: 250,
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  blurRadius: 4,
                                  color: Colors.black12.withOpacity(.2),
                                  offset: Offset(2, 2))
                            ],
                            gradient: LinearGradient(
                                colors: [Colors.blue, Colors.blueGrey])),
                        child: Text('Confirm',
                            style: TextStyle(
                                color: Colors.white.withOpacity(.8),
                                fontSize: 18,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                ]),
              ),
            ),
          ),
        ),
      ),
    );
  }
  Future priceOrder(String price) async {
    Message response =
          await Provider.of<Auth>(context, listen: false).price(this.widget.id ,price);

    if(response.status) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => CustomerInfo(),
        ),);
    }
  }

}
