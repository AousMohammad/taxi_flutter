import 'package:app/services/Auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'Price.dart';

class ViewOrders extends StatefulWidget {
  @override
  State<ViewOrders> createState() => _ViewOrdersState();
}

class _ViewOrdersState extends State<ViewOrders> {
  bool isPasswordVisible = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue, Colors.blueGrey])),
          child: Consumer<Auth>(builder: (context, auth, child) {
            return ListView.builder(
                itemCount: auth.orders == null ? 0 : auth.orders.length,
                itemBuilder: (ctx, index) {
                  return Padding(
                    padding: EdgeInsets.fromLTRB(25, 50, 20, 0),
                    child: Card(
                      child: Column(mainAxisSize: MainAxisSize.max, children: [
                        ListTile(
                          leading: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                            child: Icon(
                              Icons.accessibility,
                              size: 60,
                              color: Colors.blueGrey,
                            ),
                          ),
                          title: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 20, 10, 0),
                            child: Column(
                              children: [
                                Text('start : ' + auth.orders[index].start,
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold)),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                    'destination : ' +
                                        auth.orders[index].destination,
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextButton(
                            child: Text(
                              'Accept',
                              style: TextStyle(
                                color: Colors.blue,
                                fontSize: 15,
                              ),
                            ),
                            onPressed: () {
                              // id = index;
                              // details(index);
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      Price(auth.orders[index].id),
                                ),
                              );
                            },
                          ),
                        ),
                      ]),
                    ),
                  );
                });
          }),
        ),
      ),
    );
  }
}
