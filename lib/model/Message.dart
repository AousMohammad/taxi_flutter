import 'package:app/model/user/User.dart';

class Message {
  bool status;
  String mes;
  User user;

  Message({this.status, this.mes, this.user});

  Message.fromJson(Map<String, dynamic> json)
      : status = json['status'],
        mes = json['message'],
        user = User.fromJson(json['data']);
}
