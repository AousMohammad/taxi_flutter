class Prepare {
  bool status;
  String mes;

  Prepare({this.status, this.mes,});

  Prepare.fromJson(Map<String, dynamic> json)
      : status = json['status'],
        mes = json['message'];
}
