import 'Taxi.dart';

class ListTaxi {
  bool status;
  List<Taxi> taxis;

  ListTaxi({this.taxis, this.status});

  factory ListTaxi.fromJson(Map<String, dynamic> json) => ListTaxi(
        status: json['status'],
        taxis: List<Taxi>.from(json["data"].map((x) => Taxi.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(taxis.map((x) => x.toJson())),
      };
}
