class Orders {
  int user_id;
  int driver_id;
  String start;
  String destination;

  Orders({this.user_id, this.driver_id, this.start, this.destination});

  Orders.fromJson(Map<String, dynamic> json)
      : user_id = json['user_id'],
        driver_id = json['driver_id'],
        start = json['start'],
        destination = json['destination'];
}
