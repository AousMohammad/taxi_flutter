class Taxi {
  int driver_id;
  String model;

  Taxi({this.driver_id, this.model});

  Taxi.fromJson(Map<String, dynamic> json)
      : driver_id = json['driver_id'],
        model = json['model'];

  Map<String, dynamic> toJson() => {
        "driver_id": driver_id,
        "model": model,
      };
}
