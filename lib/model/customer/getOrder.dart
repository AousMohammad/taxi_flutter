import 'package:app/model/customer/Orders.dart';

class getOrder {
  bool status;
  String message;
  Orders orders;

  getOrder({this.status, this.message, this.orders});

  getOrder.fromJson(Map<String, dynamic> json)
      : status = json['status'],
        message = json['message'],
        orders = Orders.fromJson(json['data']);
}
