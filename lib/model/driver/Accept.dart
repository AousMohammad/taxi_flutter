class Accept {
  final int id;
  final int price;
  final String start;
  final String destination;

  Accept({this.id, this.price, this.start, this.destination});

  Accept.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        price = json['price'],
        start = json['start'],
        destination = json['destination'];

  Map<String, dynamic> toJson() => {
        // id : 'id',
        // price  : 'price',
        start: 'start',
        destination: 'destination',
      };
}
