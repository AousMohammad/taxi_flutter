import 'Accept.dart';

class DriverOrders {
  bool status;
  List<Accept> orders;

  DriverOrders({this.orders, this.status});

  factory DriverOrders.fromJson(Map<String, dynamic> json) => DriverOrders(
        status: json['status'],
        orders: List<Accept>.from(json["data"].map((x) => Accept.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(orders.map((x) => x.toJson())),
      };
}
