import 'package:app/model/user/User.dart';

class Data {
  User user;
  String token;
  Data({this.user, this.token});

  Data.fromJson(Map<String, dynamic> json)
      : user = User.fromJson(json['user']),
        token = json['token'];
}
