import 'package:app/model/user/Data.dart';

class Response {
  bool status;
  String message;
  Data data;

  Response({this.status, this.message, this.data});

  Response.fromJson(Map<String, dynamic> json)
      : status = json['status'],
        message = json['message'],
        data = Data.fromJson(json['data']);
}
