class User {
  final int id;
  final String name;
  final String email;
  final String password;
  final String numberPhone;
  final int type;
  final String start;
  final String destination;

  User(
      {this.id,
      this.name,
      this.email,
      this.password,
      this.type,
      this.numberPhone,
      this.start,
      this.destination});

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        email = json['email'],
        password = json['password'],
        numberPhone = json['phone_number'],
        type = json['type'],
        start = json['start'],
        destination = json['destination'];
}
