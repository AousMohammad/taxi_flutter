import 'package:app/model/Prepare.dart';
import 'package:app/model/customer/getOrder.dart';
import 'package:app/model/customer/Taxi.dart';
import 'package:app/model/driver/Accept.dart';
import 'package:app/model/driver/DriverOrders.dart';
import 'package:app/services/Dio.dart';
import 'package:app/model/customer/ListTaxi.dart';
import 'package:app/model/user/Response.dart';
import 'package:app/model/Message.dart';
import 'package:dio/dio.dart' as Dio;
import 'package:flutter/material.dart';

class Auth extends ChangeNotifier {
  bool isLoggedIn = false;
  Response _response;
  String token;
  Prepare prepare;
  ListTaxi listTaxi;
  getOrder order;
  Message drivInfo;
  Message cusInfo;
  DriverOrders driverOrders;
  DriverOrders driverTravel;

  List<Taxi> taxis = [];
  List<Accept> travels = [];
  List<Accept> orders = [];

  bool get authentication => isLoggedIn;
  Response get response => _response;

  void tryToken({String token}) {
    if (token == null) {
      return;
    } else {
      try {
        if (_response.status) {
          this.isLoggedIn = true;
          notifyListeners();
        }
      } catch (e) {
        print(e);
      }
    }
  }

  void logout() {
    isLoggedIn = false;
    notifyListeners();
  }

  Future<Response> login({Map users}) async {
    try {
      Dio.Response response = await dio().post('/login', data: users);
      String token = response.data.toString();
      this._response = Response.fromJson(response.data);
      this.token = _response.data.token;
      this.tryToken(token: token);
      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return _response;
    } catch (e) {
      print(e);
    }
  }

  Future<Response> register({Map users}) async {
    try {
      Dio.Response response = await dio().post('/register', data: users);
      this._response = Response.fromJson(response.data);
      this.token = _response.data.token;
      notifyListeners();
      print(
          "========================================================================================");
      print(response.data.toString());
      print(
          "========================================================================================");
      return _response;
    } catch (e) {
      print(e);
    }
  }

  Future<ListTaxi> customer_get_nearly_cars() async {
    try {
      final Dio.Response response = await dio().get('/customer/get_nearly_cars',
          options: Dio.Options(headers: {'Authorization': 'Bearer $token'}));

      this.listTaxi = ListTaxi.fromJson(response.data);
      taxis = listTaxi.taxis;

      print(listTaxi);

      notifyListeners();

      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return listTaxi;
    } catch (e) {
      print(e);
    }
  }

  Future<getOrder> customer_order_taxi({Map ord}) async {
    Dio.Response response = await dio().post('/customer/order_taxi',
        data: ord,
        options: Dio.Options(headers: {'Authorization': 'Bearer $token'}));

    this.order = getOrder.fromJson(response.data);
    notifyListeners();
    print(
        "========================================================================================");
    print(response.data.toString());
    print(
        "========================================================================================");
    return order;
  }

  Future<Prepare> perpare_account({Map loc}) async {
    try {
      Dio.Response response = await dio().post('/account',
          data: loc,
          options: Dio.Options(headers: {'Authorization': 'Bearer $token'}));

      this.prepare = Prepare.fromJson(response.data);
      notifyListeners();
      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return prepare;
    } catch (e) {
      print(e);
    }
  }

  Future<Prepare> driver_free({Map free}) async {
    try {
      Dio.Response response = await dio().post('/driver/free?key=1',
          data: free,
          options: Dio.Options(headers: {'Authorization': 'Bearer $token'}));

      Prepare pre = Prepare.fromJson(response.data);
      print(pre);
      notifyListeners();

      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return pre;
    } catch (e) {
      print(e);
    }
  }

  Future<Message> price(int id, String price) async {
    try {
      Dio.Response response = await dio().post(
          '/customer/order/$id?price=$price',
          options: Dio.Options(headers: {'Authorization': 'Bearer $token'}));
      this.cusInfo = Message.fromJson(response.data);

      print(cusInfo);
      notifyListeners();

      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return cusInfo;
    } catch (e) {
      print(e);
    }
  }

  Future<Message> driver_info(int id) async {
    try {
      Dio.Response response = await dio().get('/driver/$id',
          options: Dio.Options(headers: {'Authorization': 'Bearer $token'}));
      this.drivInfo = Message.fromJson(response.data);
      print(drivInfo);

      notifyListeners();

      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return drivInfo;
    } catch (e) {
      print(e);
    }
  }

  Future<DriverOrders> driver_Orders() async {
    try {
      Dio.Response response = await dio().get('/driver/list/orders',
          options: Dio.Options(headers: {'Authorization': 'Bearer $token'}));

      this.driverOrders = DriverOrders.fromJson(response.data);

      orders = driverOrders.orders;
      print(driverOrders);

      notifyListeners();

      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return driverOrders;
    } catch (e) {
      print(e);
    }
  }

  Future<DriverOrders> driver_travel() async {
    try {
      Dio.Response response = await dio().get('/driver/travels',
          options: Dio.Options(headers: {'Authorization': 'Bearer $token'}));

      this.driverTravel = DriverOrders.fromJson(response.data);

      travels = driverTravel.orders;
      print(driverTravel);

      notifyListeners();

      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return driverTravel;
    } catch (e) {
      print(e);
    }
  }

  Future<Prepare> delete_travel(int id) async {
    try {
      Dio.Response response = await dio().delete('/driver/travels/$id',
          options: Dio.Options(headers: {'Authorization': 'Bearer $token'}));

      Prepare prep = Prepare.fromJson(response.data);

      print(prep);
      notifyListeners();

      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return prep;
    } catch (e) {
      print(e);
    }
  }

}
