import 'package:dio/dio.dart';

Dio dio() {
  Dio dio = new Dio();

  // dio.options.baseUrl = "https://morning-river-58919.herokuapp.com/api";

  dio.options.baseUrl = "http://192.168.5.121:8000/api";

  dio.options.headers['Accept'] = "application/json";

  return dio;
}
